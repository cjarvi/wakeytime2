# README #

### What is this repository for? ###

WakeyTime2 is an exercise in familiarizing myself with the ReactiveSwift library and FRP concepts by developing a working
alarm clock.

Current status:
The inner workings of the alarm clock are fully functional.  However, the UI only displays the current time.  
Additional work is needed to add UI for displaying when the alarm fires and for silencing it when it does.  Also needed
is a sound manager to play an audible alarm.

### How do I get set up? ###

The project uses CocoaPods.  Do a `pod install` in the root of the project.

### Contribution guidelines ###

No external contribution allowed at this point.
