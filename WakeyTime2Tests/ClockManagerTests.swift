//
//  ClockManagerTests.swift
//  WakeyTime2Tests
//
//  Created by Chris Jarvi on 11/23/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import XCTest
@testable import WakeyTime2
import ReactiveSwift
import Result

class ClockManagerTests: XCTestCase {
    
    private var clockManagerUT: ClockManager? = ClockManager()
    private var signalsReceived: [TimeData] = []
    private var disposable = CompositeDisposable()
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        signalsReceived.removeAll()
        disposable.dispose()
        clockManagerUT = nil
        super.tearDown()
    }
    
    func testStartClockShouldSendSignalEverySecond() {
        guard let clockManagerUT = self.clockManagerUT else {
            XCTFail()
            return
        }
        let waitExpectation = expectation(description: "time signals collected expectation")
        
        disposable += clockManagerUT.currentTimeSignal
            .observeValues { (timeData) in
                self.signalsReceived.append(timeData)
                if self.signalsReceived.count == 6 {
                    waitExpectation.fulfill()
                }
            }
        
        clockManagerUT.startClock()
        waitForExpectations(timeout: 10) { (error) in
            self.disposable.dispose()
            guard error == nil else { return }
            guard let first = self.signalsReceived.first, let last = self.signalsReceived.last else {
                XCTFail()
                return
            }
            let timeDiff = last.currentDate.timeIntervalSince(first.currentDate)
            if timeDiff < 4.5 || timeDiff >= 5.5 {
                XCTFail("Time difference was outside the expected range")
                return
            }
            
        }
    }
    
    func testClockManagerSendsTimeData() {
        guard let clockManagerUT = self.clockManagerUT else {
            XCTFail()
            return
        }
        let signalExpectation = expectation(description: "Signal recieved expectation")
        let unitFlags = Set<Calendar.Component>([.hour,
                                                 .minute,
                                                 .second,
                                                 .day,
                                                 .weekday,
                                                 .weekdayOrdinal,
                                                 .month,
                                                 .year])
        let components = Calendar.autoupdatingCurrent.dateComponents(unitFlags, from: Date())
        clockManagerUT.startClock()
        
        disposable += clockManagerUT.currentTimeSignal
            .take(first: 1)
            .observeValues { [weak self] (timeData) in
                self?.signalsReceived.append(timeData)
                signalExpectation.fulfill()
            }
        
        waitForExpectations(timeout: 5) { (error) in
            self.disposable.dispose()
            if let error = error {
                XCTFail(error.localizedDescription)
                return
            }
            
            guard let first = self.signalsReceived.first else {
                XCTFail("No signals were received")
                return
            }
            XCTAssertEqual(first.day, components.day)
        }
        
    }
}
