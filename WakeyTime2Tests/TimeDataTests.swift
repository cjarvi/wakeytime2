//
//  TimeDataTests.swift
//  WakeyTime2Tests
//
//  Created by Chris Jarvi on 11/23/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import XCTest
@testable import WakeyTime2

class TimeDataTests: XCTestCase {
    
    private var testDate: Date!
    private let month = 11
    private let day = 23
    private let year = 2017
    private let hour = 14
    private let minute = 10
    private let second = 23
    
    override func setUp() {
        super.setUp()
        var components = DateComponents()
        components.month = month
        components.day = day
        components.year = year
        components.hour = hour
        components.minute = minute
        components.second = second
        components.timeZone = Calendar.autoupdatingCurrent.timeZone
        self.testDate = Calendar.autoupdatingCurrent.date(from: components)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTimeFromDate() {
        let timeData = TimeData(date: self.testDate)
        XCTAssertEqual(timeData.twentyFourHour, hour)
        XCTAssertEqual(timeData.hour, hour - 12)
        XCTAssertEqual(timeData.meridiem, "PM")
        XCTAssertEqual(timeData.minute, minute)
        XCTAssertEqual(timeData.second, second)
    }
    
    func testDateDataFromDate() {
        let dateData = TimeData(date: self.testDate)
        XCTAssertEqual(dateData.day, day)
        XCTAssertEqual(dateData.month, month)
        XCTAssertEqual(dateData.year, year)
    }
    
    func testComputedDataFromDate() {
        let dateData = TimeData(date: self.testDate, calendar: Calendar.current)
        
        XCTAssertEqual(dateData.weekday, 5)
        XCTAssertEqual(dateData.weekdayOrdinal, 4)
        XCTAssertEqual(dateData.weekdayName, "Thursday")
        XCTAssertEqual(dateData.weekdayNameMedium, "Thu")
        XCTAssertEqual(dateData.weekdayNameShort, "T")
        XCTAssertEqual(dateData.monthName, "November")
        XCTAssertEqual(dateData.monthNameShort, "Nov")
    }
}
