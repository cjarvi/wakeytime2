//
//  AlarmManagerTests.swift
//  WakeyTime2Tests
//
//  Created by Chris Jarvi on 12/7/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import XCTest
import ReactiveSwift
import Result
@testable import WakeyTime2

class AlarmManagerTests: XCTestCase {
    
    var AlarmManagerUT: AlarmManager!
    var currentTimeSignal: Signal<TimeData, NoError>!
    var currentTimeObserver: Signal<TimeData, NoError>.Observer!
    var disposables = CompositeDisposable()
    
    override func setUp() {
        super.setUp()
        
        let (currentTimeSignal, currentTimeObserver) = Signal<TimeData, NoError>.pipe(disposable: disposables)
        self.currentTimeSignal = currentTimeSignal
        self.currentTimeObserver = currentTimeObserver
        AlarmManagerUT = AlarmManager(currentTimeSignal: currentTimeSignal)
    }
    
    override func tearDown() {
        currentTimeObserver.sendCompleted()
        disposables.dispose()
        super.tearDown()
    }
    
    func testAlarmManagerSignalsWithMatchingTime() {
        let testDate = Date(timeIntervalSince1970: 1513159877000)
        let alarm = TimeData(date: testDate, calendar: Calendar.current)
        AlarmManagerUT.update(alarm: alarm)
        AlarmManagerUT.enable()
        
        let alarmFiredExpectation = expectation(description: "Alarm did fire")
        AlarmManagerUT.alarmSignal
            .filter { $0.event == .Fired }
            .observeValues { _ in
                alarmFiredExpectation.fulfill()
        }
        
        currentTimeObserver.send(value: alarm)
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testAlarmManagerDoesNotSignalWithUnmatchedTime() {
        let testDate = Date(timeIntervalSince1970: 1513159877000)
        let testTimeData = TimeData(date: testDate, calendar: Calendar.current)
        let alarmDate = Date(timeIntervalSince1970: 1513159200000)
        let alarm = TimeData(date: alarmDate, calendar: Calendar.current)
        AlarmManagerUT.update(alarm: alarm)
        AlarmManagerUT.enable()
        
        let alarmFiredExpectation = expectation(description: "Alarm did fire")
        alarmFiredExpectation.isInverted = true
        
        AlarmManagerUT.alarmSignal
            .filter { $0.event == .Fired }
            .observeValues { _ in
                alarmFiredExpectation.fulfill()
            }
        
        currentTimeObserver.send(value: testTimeData)
        
        waitForExpectations(timeout: 0.5, handler: nil)
    }
    
    func testAlarmManagerDisabledDoesNotSignal() {
        let testDate = Date(timeIntervalSince1970: 1513159877000)
        let alarm = TimeData(date: testDate, calendar: Calendar.current)
        AlarmManagerUT.update(alarm: alarm)
        AlarmManagerUT.disable()
        
        let alarmFiredExpectation = expectation(description: "Alarm did fire")
        alarmFiredExpectation.isInverted = true
        
        AlarmManagerUT.alarmSignal
            .filter { $0.event == .Fired }
            .observeValues { _ in
                alarmFiredExpectation.fulfill()
            }
        
        currentTimeObserver.send(value: alarm)
        
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testAlarmCanBeDismissed() {
        let testDate = Date(timeIntervalSince1970: 1513159877000)
        let alarm = TimeData(date: testDate, calendar: Calendar.current)
        AlarmManagerUT.update(alarm: alarm)
        AlarmManagerUT.enable()
        
        let alarmSilencedExpectation = expectation(description: "Alarm was silenced")
        
        AlarmManagerUT.alarmSignal
            .filter { $0.event == .Silenced }
            .observeValues { _ in
                alarmSilencedExpectation.fulfill()
            }
        
        currentTimeObserver.send(value: alarm)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) { [weak self] in
            self?.AlarmManagerUT.turnOff()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
    }
}
