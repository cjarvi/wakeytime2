//
//  ClockHand.swift
//  WakeyTime2
//
//  Created by Chris Jarvi on 12/30/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import SpriteKit

public class ClockHand: SKSpriteNode {
    let clockCenter: CGPoint
    
    public init(clockCenter: CGPoint, texture: SKTexture?, color: SKColor, size: CGSize) {
        self.clockCenter = clockCenter
        super.init(texture: texture, color: color, size: size)
        configure()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented")
    }
    
    private func configure() {
        self.anchorPoint = CGPoint(x: size.width / 2, y: 1)
        self.blendMode = .alpha
        let distanceRange = SKRange(constantValue: CGFloat(CGFloat.pi / 2))
        let orientationConstraint = SKConstraint.orient(to: clockCenter, offset: distanceRange)
        constraints = [orientationConstraint]
    }
}
