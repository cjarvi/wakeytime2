//
//  ClockScene.swift
//  WakeyTime2
//
//  Created by Chris Jarvi on 12/30/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import ReactiveSwift
import SpriteKit
import UIKit

public class ClockScene: SKScene {
    let disposable = CompositeDisposable()
    let centerNode = SKSpriteNode(color: SKColor.yellow, size: CGSize(width: 1, height: 1))
    var hourHand: SKSpriteNode?
    var minuteHand: SKSpriteNode?
    var secondHand: SKSpriteNode?
    let angleOffset = 90.0
    
    public override func didMove(to view: SKView) {
        centerNode.position = sceneMidPoint()
        scene?.addChild(centerNode)
        generateClockFace()
        scene?.backgroundColor = SKColor.black
        
        disposable += AppDelegate.runtime.clockManager.currentTimeSignal.observeValues { [weak self] timeData in
            self?.updateSecondHand(timeData)
            self?.updateMinuteHand(timeData)
            self?.updateHourHand(timeData)
        }
    }
    
    deinit {
        disposable.dispose()
    }
    
    private func updateSecondHand(_ timeData: TimeData) {
        let angle = Double(timeData.second) * -6.0 + angleOffset
        let position = circularCoordinate(angle: angle, radius: tickDistance, center: sceneMidPoint())
        if secondHand == nil {
            generateSecondHand()
        }
        secondHand?.position = position
    }
    
    private func updateMinuteHand(_ timeData: TimeData) {
        let angle = Double(timeData.minute) * -6.0 + angleOffset
        let position = circularCoordinate(angle: angle, radius: tickDistance, center: sceneMidPoint())
        if minuteHand == nil {
            generateMinuteHand()
        }
        minuteHand?.position = position
    }
    
    private func updateHourHand(_ timeData: TimeData) {
        let angle = -(360.0 / 12.0) * Double(timeData.hour) + angleOffset
        let position = circularCoordinate(angle: angle, radius: tickDistance * 0.75, center: sceneMidPoint())
        if hourHand == nil {
            generateHourHand()
        }
        hourHand?.position = position
    }
    
    private func generateClockFace() {
        let numberOfTicks = 12
        let baseAngle = 360.0 / Double(numberOfTicks)
        for tick in 1...numberOfTicks {
            let shape = generateTickNode(color: SKColor.red)
            shape.position = circularCoordinate(angle: baseAngle * Double(tick), radius: tickDistance, center: sceneMidPoint())
            scene?.addChild(shape)
        }
    }
    
    private func generateSecondHand() {
        let handSize = CGSize(width: 1.0, height: tickDistance)
        self.secondHand = ClockHand(clockCenter: sceneMidPoint(),
                              texture: nil,
                              color: SKColor.cyan,
                              size: handSize)
        if let secondHand = secondHand, let scene = scene {
            scene.addChild(secondHand)
        }
    }
    
    private func generateMinuteHand() {
        let handSize = CGSize(width: 2.0, height: tickDistance)
        self.minuteHand = ClockHand(clockCenter: sceneMidPoint(),
                                    texture: nil,
                                    color: SKColor.green,
                                    size: handSize)
        if let minuteHand = minuteHand, let scene = scene {
            scene.addChild(minuteHand)
        }
    }
    
    private func generateHourHand() {
        let handSize = CGSize(width: 2.0, height: tickDistance * 0.75)
        self.hourHand = ClockHand(clockCenter: sceneMidPoint(),
                                  texture: nil,
                                  color: SKColor.green,
                                  size: handSize)
        if let hourHand = hourHand, let scene = scene {
            scene.addChild(hourHand)
        }
    }
    
    private func sceneMidPoint() -> CGPoint {
        return CGPoint(x: size.width / 2, y: size.height / 2)
    }
    
    private func generateTickNode(color: SKColor) -> SKShapeNode {
        let minSceneSize = Swift.min(size.height, size.width)
        let tickHeight = minSceneSize * 0.05
        let tickWidth: CGFloat = 3.0
        
        let shapeNode = SKShapeNode(rectOf: CGSize(width: tickWidth, height: tickHeight))
        shapeNode.fillColor = color
        shapeNode.strokeColor = color
        
        let distanceRange = SKRange(constantValue: CGFloat(-CGFloat.pi / 2))
        let orientationConstraint = SKConstraint.orient(to: sceneMidPoint(), offset: distanceRange)
        shapeNode.constraints = [orientationConstraint]
        
        return shapeNode
    }
    
    private func circularCoordinate(angle: Double, radius: Double, center: CGPoint) -> CGPoint {
        let radians = angle * .pi / 180.0
        let x = Double(center.x) + radius * cos(radians)
        let y = Double(center.y) + radius * sin(radians)
        
        return CGPoint(x: x, y: y)
    }
    
    private var tickDistance: Double {
        let minSceneSize = Swift.min(size.height, size.width)
        let buffer = minSceneSize * 0.05
        return Double((minSceneSize / 2.0) - buffer)
    }
}
