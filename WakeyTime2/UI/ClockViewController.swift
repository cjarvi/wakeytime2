//
//  ViewController.swift
//  WakeyTime2
//
//  Created by Chris Jarvi on 11/16/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import UIKit
import SpriteKit

class ClockViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let clockScene = ClockScene(size: UIScreen.main.bounds.size)
        let skView = self.view as! SKView
        skView.ignoresSiblingOrder = true
        clockScene.scaleMode = .resizeFill
        skView.presentScene(clockScene)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prefersHomeIndicatorAutoHidden() -> Bool {
        return true
    }
    
    
}

