//
//  Runtime.swift
//  WakeyTime2
//
//  Created by Chris Jarvi on 12/13/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import Foundation

public struct Runtime {
    public private(set) var clockManager: ClockManager
    public private(set) var alarmManager: AlarmManager
    
    init() {
        clockManager = ClockManager()
        alarmManager = AlarmManager(currentTimeSignal: clockManager.currentTimeSignal)
    }
}
