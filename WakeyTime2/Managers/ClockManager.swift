//
//  ClockManager.swift
//  WakeyTime2
//
//  Created by Chris Jarvi on 11/16/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

public class ClockManager {
    public private(set) var currentTimeSignal: Signal<TimeData, NoError>
    private var currentTimeObserver: Signal<TimeData, NoError>.Observer
    private var clockTimer: Timer?
    private var disposables = CompositeDisposable()
    
    init() {
        (currentTimeSignal, currentTimeObserver) = Signal<TimeData, NoError>.pipe(disposable: disposables)
        
//        disposables += currentTimeSignal.observeValues { (timeData) in
//            print(timeData.shortDescription())
//        }
    }
    
    deinit {
        disposables.dispose()
    }
    
    func startClock() {
        self.clockTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] _ in
            self?.currentTimeObserver.send(value: TimeData())
        }
    }
    
    func stopClock() {
        currentTimeObserver.sendCompleted()
        clockTimer?.invalidate()
        clockTimer = nil
    }
}
