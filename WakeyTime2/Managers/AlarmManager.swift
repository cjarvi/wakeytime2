//
//  AlarmManager.swift
//  WakeyTime2
//
//  Created by Chris Jarvi on 12/7/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

public class AlarmManager {
    public private(set) var alarmSignal: Signal<AlarmEvent, NoError>
    private var alarmObserver: Signal<AlarmEvent, NoError>.Observer
    private var disposables = CompositeDisposable()
    public private(set) var alarmData: TimeData?
    private var enabled: Bool = false
    private var isFiring: Bool = false
    
    init(currentTimeSignal: Signal<TimeData, NoError>) {
        (alarmSignal, alarmObserver) = Signal<AlarmEvent, NoError>.pipe(disposable: disposables)
        
        disposables += currentTimeSignal
            .filter { [weak self] _ in
                guard let strongSelf = self else { return false }
                return strongSelf.enabled
            }
            .filter { [weak self] currentTime in
                return currentTime == self?.alarmData
            }
            .observeValues { [weak self] _ in
                self?.isFiring = true
                let event = AlarmEvent(event: .Fired)
                self?.alarmObserver.send(value: event)
            }
    }
    
    deinit {
        alarmObserver.sendCompleted()
        disposables.dispose()
    }
    
    func update(alarm: TimeData) {
        alarmData = alarm
        let event = AlarmEvent(event: .Updated)
        alarmObserver.send(value: event)
    }
    
    func enable() {
        enabled = true
        let event = AlarmEvent(event: .Enabled)
        alarmObserver.send(value: event)
    }
    
    func disable() {
        enabled = false
        let event = AlarmEvent(event: .Disabled)
        alarmObserver.send(value: event)
    }
    
    func turnOff() {
        isFiring = false
        let event = AlarmEvent(event: .Silenced)
        alarmObserver.send(value: event)
    }
}
