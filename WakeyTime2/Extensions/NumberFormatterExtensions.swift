//
//  NumberFormatterExtensions.swift
//  WakeyTime2
//
//  Created by Chris Jarvi on 12/14/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import Foundation

extension BinaryFloatingPoint {
    static public func formatNumber(_ value: NSNumber, style: NumberFormatter.Style, decimals: Int = 0) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = style
        formatter.maximumFractionDigits = decimals
        
        return formatter.string(from: value) ?? ""
    }
}


