//
//  AlarmEvent.swift
//  WakeyTime2
//
//  Created by Chris Jarvi on 12/13/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import Foundation

public struct AlarmEvent {
    public enum AlarmEventType {
        case Disabled
        case Enabled
        case Fired
        case Silenced
        case Updated
    }
    
    public let event: AlarmEventType
    public let timeStamp: Date
    
    public init(event: AlarmEventType) {
        self.event = event
        self.timeStamp = Date()
    }
}
