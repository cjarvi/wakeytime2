//
//  TimeData.swift
//  WakeyTime2
//
//  Created by Chris Jarvi on 11/23/17.
//  Copyright © 2017 Chris Jarvi. All rights reserved.
//

import Foundation

public struct TimeData : Equatable {
    public let currentDate: Date
    public let calendar: Calendar
    public let timeZone: TimeZone
    public let twentyFourHour: Int
    public let hour: Int
    public let minute: Int
    public let second: Int
    public let meridiem: String
    public let weekday: Int // i.e. Sunday, Monday, Tuesday, etc.
    public let weekdayOrdinal: Int // i.e. Second Sunday of the month
    public let weekdayName: String
    public let weekdayNameMedium: String
    public let weekdayNameShort: String
    public let month: Int
    public let monthName: String
    public let monthNameShort: String
    public let day: Int
    public let year: Int
    
    init(date: Date = Date(), calendar: Calendar = Calendar.autoupdatingCurrent) {
        self.currentDate = date
        self.calendar = calendar
        self.timeZone = calendar.timeZone
        let unitFlags = Set<Calendar.Component>([.hour,
                                                 .minute,
                                                 .second,
                                                 .day,
                                                 .weekday,
                                                 .weekdayOrdinal,
                                                 .month,
                                                 .year])
        let components = self.calendar.dateComponents(unitFlags, from: self.currentDate)
        self.twentyFourHour = components.hour ?? 0
        self.minute = components.minute ?? 0
        self.second = components.second ?? 0
        self.day = components.day ?? 0
        self.month = components.month ?? 0
        self.year = components.year ?? 0
        self.weekday = components.weekday ?? 0
        self.weekdayOrdinal = components.weekdayOrdinal ?? 0
        if self.twentyFourHour > 12 {
            self.hour = self.twentyFourHour - 12
            self.meridiem = "PM"
        } else {
            self.hour = self.twentyFourHour
            self.meridiem = "AM"
        }
        //weekday and month indices start from 1
        //arrays and collections are 0 based
        let unitOffset = -1
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = self.calendar
        self.weekdayName = dateFormatter.weekdaySymbols[self.weekday + unitOffset]
        self.weekdayNameMedium = dateFormatter.shortWeekdaySymbols[self.weekday + unitOffset]
        self.weekdayNameShort = dateFormatter.veryShortWeekdaySymbols[self.weekday + unitOffset]
        self.monthName = dateFormatter.monthSymbols[self.month + unitOffset]
        self.monthNameShort = dateFormatter.shortMonthSymbols[self.month + unitOffset]
    }
    
    public static func ==(lhs: TimeData, rhs: TimeData) -> Bool {
        return lhs.twentyFourHour == rhs.twentyFourHour && lhs.minute == rhs.minute
    }
    
    public func shortDescription() -> String {
        return "\(hour):\(minute):\(second) \(meridiem)"
    }
}
